# Contributing
When contributing to this repository, please make a branch for fixes, features, or anything else and make a merge request and do not commit directly to `main` branch.

# Development Environment
## Clone
Generate an SSH keypair and add the public key to [SSH Keys](https://gitlab.com/-/profile/keys):
```
username@hostname:~$ ssh-keygen
username@hostname:~$ cat .ssh/id_sha.pub
```
Configure git and then clone this repository via SSH

